import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { REALM_CONFIG } from '../environments/environment';
import { RealmConnectModule } from '@pintado/realm-connect';
import { RealmAuthModule } from '@pintado/realm-auth';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RealmConnectModule.forRoot(REALM_CONFIG),
    RealmAuthModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
