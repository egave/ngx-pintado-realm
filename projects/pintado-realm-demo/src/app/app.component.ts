import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { RealmConnectService } from '@pintado/realm-connect';
import { RealmAuthService } from '@pintado/realm-auth';
import { MONGO_CONFIG } from '../environments/environment';

@Component({
  selector: 'pintado-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'pintado-realm-demo';

  public products: any | undefined = undefined;
  public isAnonymous: boolean | undefined = undefined;

  constructor(public realmConnectSrvc : RealmConnectService,
              public realmAuthService : RealmAuthService,
              private ref: ChangeDetectorRef) { }

  async ngOnInit() {
      /* Setting the Default Database */
      await this.realmConnectSrvc.setDefaultDb(MONGO_CONFIG.DB.NAME);
      /* Fetch a collection */
      this.products = await this.realmConnectSrvc.fetchCollection(MONGO_CONFIG.DB.COLLECTIONS.PRODUCTS);
      this.ref.detectChanges();
      console.log("found " + this.products.length + " products");

      this.isAnonymous = await this.realmAuthService.isAnonymous();
      this.ref.detectChanges();
      console.log("Is connected as anonymous : " + this.isAnonymous);
      /* Get a connection to a different Database than the default one */
      // const newDb = await this.mongoRealmSrvc.getDbConnection(MONGO_CONFIG.DB.NAME);
      /* Use the new connection to query a collection */
      // this.products = await newDb.collection(MONGO_CONFIG.DB.COLLECTIONS.PRODUCTS).find();
  }
}
