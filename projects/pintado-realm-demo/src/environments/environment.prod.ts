export const environment = {
  production: true
};

export const REALM_CONFIG = {
  appConfig: {
    id: 'ionicmongostarter_dev-oycop',
    timeout: 10000
  },
  serviceName: 'mongodb-atlas'
};

export const MONGO_CONFIG = {
  DB: {
    NAME : 'ionicMongoStarter',
    COLLECTIONS : {
      PRODUCTS : 'products'
    }
  }
};
