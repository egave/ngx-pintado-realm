# Clean up previous distributions
rm -rf dist
rm -rf build

# Variable pointing to NGC
NGC="node ../../node_modules/.bin/ngc"

#Run Angular Compilar
$NGC -p tsconfig.lib.json

cp src/package.json dist/package.json
