import { Injectable, Inject } from '@angular/core';
import { IRealmConfig, RealmConfigService } from './realm-connect.module';
import * as Realm from 'realm-web';

/**
  * MongoRealmService provides Mongo Db Atlas / Realm connection functions and Database utilities.
  *
  */

@Injectable({
  providedIn: 'root'
})
export class RealmConnectService {

  private readonly realmApp: Realm.App = new Realm.App(this.config.appConfig);
  private readonly mongoServiceName = this.config.serviceName;

  private anomUser: Realm.User | undefined = undefined;
  private mongoService: globalThis.Realm.Services.MongoDB | undefined = undefined;
  private defaultDb: globalThis.Realm.Services.MongoDBDatabase | undefined = undefined;

  /**
  * MongoRealmService constructor injects the IRealmConfig to create a new RealmApp.
  *
  */
  constructor(@Inject(RealmConfigService) private config: IRealmConfig) {
    console.debug('RealmConnectService::constructor() | method called');
    console.debug('RealmConnectService::constructor() | mongoClient initialized: ', this.getRealmApp());
  }

  /**
  * @desc This function returns the Realm.App object
  * @return Realm.App
  */
  public getRealmApp(): Realm.App {
    return this.realmApp;
  }

  /**
  * @desc This function logs into realmApp with anonymous credentials
  *
  */
  private async loginAnonymously() {
    // Create an anonymous credential
    const credentials = Realm.Credentials.anonymous();
    try {
      // Authenticate the user
      this.anomUser = await this.realmApp.logIn(credentials);
      // `App.currentUser` updates to match the logged in user
    } catch(err) {
      console.error("Failed to log in anonymously to Realm app", err);
    }
  }

  /**
  * @desc This function inits a MongoDB service. If necessary it logs into realmApp
  * with anonymous credentials before retrieving the MongoDB Service.
  *
  */
  public async initMongoService() {
    if (! this.getRealmApp().currentUser)
      await this.loginAnonymously();

    try {
      this.mongoService = this.getRealmApp().currentUser?.mongoClient(this.mongoServiceName);
      console.debug('Initialized Mongo-DB : ', this.mongoService);
    }
    catch (error){
      console.error(error + ' while getting mongoService connection to the Database');
    }
  }

  /**
  * @desc Sets a default DB Connection. If necessary it initializes a MongoDB service.
  *
  */
  public async setDefaultDb(dbName: string) {
    if (! this.mongoService)
      await this.initMongoService();

    try {
      this.defaultDb = this.getRealmApp().currentUser?.mongoClient(this.mongoServiceName).db(dbName);
      console.debug('Got a DB connection : ', this.defaultDb);
    }
    catch (error){
      console.error(error + ' while getting mongodb connection to the Database');
    }
  }

  /**
  * @desc Get a DB Connection. If necessary it initializes a MongoDB service.
  * @param dbName The name of the DB to get a connection for.
  */
  public async getDbConnection(dbName: string) : Promise<globalThis.Realm.Services.MongoDBDatabase | undefined> {
    if (! this.mongoService)
      await this.initMongoService();

    console.debug('Returning a connection to Database: `'+ dbName+'`');
    return this.mongoService?.db(dbName);
  }


  /**
    * Database Functions
  */

  /**
  * @desc Fetches the documents of a collection from the provided MongoDBDatabase or,
  * if not provided, the default Database.
  * @param collection The name of the collection to get the documents from.
  * @param dbConnection? The MongoDBDatabase to use for this query.
  * @returns an array of JSONs with all collection's documents.
  */
  fetchCollection(collection: string, dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined) : any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

    return _db?.collection(collection).find()
      .then((docs: any) => {
        console.log('MongoRealmService::fetchCollection | docs:',docs);
        return docs;
      }).catch((err: any) => {
          console.error(err);
      });
  }

  /**
  * @desc Finds the documents of a collection that matches the filter from the
  * provided MongoDBDatabase or, if not provided, the default Database.
  * @param collection The name of the collection to get the documents from.
  * @param filter The filter query.
  * @returns an array of JSONs with all collection's documents matching the filter.
  *
  */
   find(collection: string, filter: any,
        dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined) : any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

    return _db?.collection(collection).find(filter)
      .then((docs: any) => {
        console.log('MongoRealmService::find | docs:',docs);
        return docs;
      }).catch((err: any) => {
          console.error(err);
      });
  }

  /**
  * @desc Finds the documents of a collection from the provided MongoDBDatabase or,
  * if not provided, the default Database and returns a number of documents limited
  * by the 'limit' parameter.
  * @param collection The name of the collection to get the documents from.
  * @param filter The filter query.
  * @param limit The maximum number of documents returned by the query.
  * @returns an array of JSONs with all collection's documents matching the filter with
  * a maximum of a number of documents expressed by the 'limit' parameter.
  *
  */
  findAndLimit(collection: string, filter: any, limit: number,
              dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined) : any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

    return _db?.collection(collection).find(filter, { limit: limit})
    .then((docs: any) => {
      console.log('MongoRealmService::findAndLimit | docs:',docs);
      return docs;
    }).catch((err: any) => {
        console.error(err);
    });
  }

  /**
  * @desc Finds one document of a collection from the provided MongoDBDatabase or,
  * if not provided, the default Database by its id.
  * @param collection The name of the collection to get the document from.
  * @param id The id of the document to retrieve.
  * @returns a JSON with the collection's document matching the query
  *
  */
  findOne(collection: string, id: any,
          dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined) : any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

    return _db?.collection(collection).findOne({$id : id})
    .then((doc: any) => {
      console.debug('MongoRealmService::findOne | doc:',doc);
      return doc;
    }).catch((err: any) => {
        console.error(err);
    });
  }

  /**
  * @desc Inserts an array of documents into a collection from the provided MongoDBDatabase or,
  * if not provided, the default Database.
  * @param collection The name of the collection to insert the documents into.
  * @param id The id of the document to retrieve.
  * @returns the list of insertedIds
  *
  */
   insertMany(collection: string, docs: any,
    dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined) : any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

     return _db?.collection(collection).insertMany(docs)
      .then((results: { insertedIds: any; }) => {
        const { insertedIds } = results;
        console.debug(insertedIds);
        return insertedIds;
      }).catch((err: any) => {
        console.error(err);
      });
  }

  /**
  * @desc Updates the documents of a collection from the provided MongoDBDatabase or,
  * if not provided, the default Database.
  * @param collection The name of the collection to update the documents from.
  * @param filter The filter query.
  * @param action The action to apply to the filtered documents.
  * @param options? The options for the update.
  *
  */
  updateMany(collection: string, filter: any, action: any, options?: any,
    dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined): any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

    return _db?.collection(collection).updateMany(filter, action, options)
    .then((_opResult: globalThis.Realm.Services.MongoDB.UpdateResult<any>) => {
      console.debug(_opResult);
      return _opResult;
    }).catch((err: any) => {
      console.error(err);
    });;
  }

  /**
  * @desc Deletes the documents of a collection from the provided MongoDBDatabase or,
  * if not provided, the default Database.
  * @param collection The name of the collection to delete the documents from.
  * @param filter The filter query.
  *
  */
   deleteMany(collection: string, filter: any,
          dbConnection?: globalThis.Realm.Services.MongoDBDatabase | undefined) : any {
    const _db = dbConnection ? dbConnection : this.defaultDb;

    return _db?.collection(collection).deleteMany(filter)
      .then((_opResult: globalThis.Realm.Services.MongoDB.DeleteResult) => {
        console.debug(_opResult);
        return _opResult;
      }).catch((err: any) => {
        console.error(err);
      });
  }

}

