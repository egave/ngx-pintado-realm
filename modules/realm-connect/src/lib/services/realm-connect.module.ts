import { ModuleWithProviders, NgModule, Optional, SkipSelf, InjectionToken } from '@angular/core';

/**
  * IRealmConfig : Definition of the REALM Config Interface
  *
  */
export interface IRealmConfig {
  appConfig: {
    readonly id: string;
    readonly timeout: number;
  };
  serviceName: string
}

export const RealmConfigService = new InjectionToken<IRealmConfig>('RealmConfig');

/**
  * This module provides :
  *   - a 'forRoot' method to initialize the Realm application when importing it in your application
  *   - MongoRealmService provider with a set of methods to deal with Database connections & queries
  *
  * To use this library :
  * 1/ import the library into 'AppModule' :
  * import { MongoRealmModule } from '@pintado/realm-connect;
  * 2/ Call the 'forRoot' static method to load the Realm Config params and init the Realm App :
  * @NgModule({
  *   imports: [
  *     MongoRealmModule.forRoot({
  *       id: '<realm-application-id>',
  *       timeout: <timeout en ms>,
  *     })
  *   ]
  * });
  * 3/ Connect to a default Database :
  * await this.mongoRealmSrvc.setDefaultDb(<database_name>);
  *    Use the utility functions in your components that uses the default Database :
  * this.docs = await this.mongoRealmSrvc.fetchCollection(<collection_name>);
  *
  * 4/ Or you can connect to another database than the one you defined as default :
  * const newDb = await this.mongoRealmSrvc.getDbConnection(<database_name>);
  * this.docs = await newDb.collection(<collection_name>).find();
  */

@NgModule({
  declarations: [],
  imports: []
})
export class RealmConnectModule {
  constructor(@Optional() @SkipSelf() parentModule?: RealmConnectModule) {
    if (parentModule) {
      throw new Error(
        'RealmConnectModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: IRealmConfig): ModuleWithProviders<RealmConnectModule> {
    return {
      ngModule: RealmConnectModule,
      providers: [
        {provide: RealmConfigService, useValue: config }
      ]
    };
  }
}
