import { TestBed } from '@angular/core/testing';

import { RealmConnectService } from './realm-connect.service';

describe('MongoRealmService', () => {
  let service: RealmConnectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RealmConnectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
