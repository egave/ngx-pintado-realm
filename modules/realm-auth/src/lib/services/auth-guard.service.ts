import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { RealmAuthService } from './realm-auth.service';


@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(public realmAuthSrvc: RealmAuthService, public router: Router) {}

  canActivate(): boolean {
    if (! this.realmAuthSrvc.isAuthenticated()) {
      window.alert ('You need to be authenticated to access this page.\n\nLogin to your account or register to create a new account.');
      return false;
    }
    return true;
  }
}
