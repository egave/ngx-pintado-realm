import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { RealmAuthService } from './realm-auth.service';


@Injectable({
  providedIn: 'root',
})
export class AnonymousGuardService implements CanActivate {
  constructor(public realmAuthSrvc: RealmAuthService, public router: Router) {}

  canActivate(): boolean {
    if (this.realmAuthSrvc.isAuthenticated()) {
      window.alert ('You are already authenticated !');
      return false;
    }
    return true;
  }
}
