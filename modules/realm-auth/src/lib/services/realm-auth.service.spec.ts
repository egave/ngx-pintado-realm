import { TestBed } from '@angular/core/testing';

import { RealmAuthService } from './realm-auth.service';

describe('RealmAuthService', () => {
  let service: RealmAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RealmAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
