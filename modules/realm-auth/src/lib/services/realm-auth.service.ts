import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
//import { filter, map, takeUntil } from 'rxjs/operators';
//import { Md5 } from 'ts-md5/dist/md5';
//import { StitchUser, StitchAuth, AnonymousCredential,
//  UserPasswordCredential, UserPasswordAuthProviderClient, StitchAuthListener} from 'mongodb-stitch-browser-sdk';
//import { User } from '../../models/user.model';
//import { UserService } from '../../services/user.service';
import { RealmConnectService } from '@pintado/realm-connect';
import * as Realm from 'realm-web';
import { User as RealmUser } from 'realm-web';

//import { MONGO } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RealmAuthService {

  //private destroy$: Subject<void> = new Subject<void>();

//  public myAuthListener: StitchAuthListener = null;
  //private currentUserSubject: BehaviorSubject<User>;
  private isConnectedSubject: BehaviorSubject<boolean | undefined>;
  //public currentUser: Observable<User>;
  public isConnected: Observable<boolean | undefined>;
  public user: RealmUser | undefined;
  /**
   * Observable of the currently signed-in user's JWT token used to identify the user to a MongoDB service (or null).
   */
  //public readonly idToken: Observable<string|null>;


  constructor(@Inject(RealmConnectService) public realmConnectSrvc: RealmConnectService,
              /*public userSrvc: UserService*/)  {
    console.log('RealmAuthService::constructor() | method called');
    this.isConnectedSubject = new BehaviorSubject<boolean | undefined>(this.isAuthenticated());
    this.isConnected = this.isConnectedSubject.asObservable();

    // Define the listener
/*
    this.myAuthListener = {
      onUserAdded: (auth, addedUser) => {
        console.log('onUserAdded:', addedUser);
      },
      onUserLoggedIn: (auth, loggedInUser) => {
        console.log('onUserLoggedIn:', loggedInUser);
        this.isConnectedSubject.next(this.isAuthenticated());
        console.log('isConnectedSubject.next:', this.isAuthenticated());
        //this.userSrvc.setStitchUser(loggedInUser);
      },
      onActiveUserChanged: (auth, currentActiveUser, previousActiveUser) => {
        console.log('onActiveUserChanged:', currentActiveUser, previousActiveUser);
        currentActiveUser ? this.userSrvc.setStitchUser(currentActiveUser) : this.userSrvc.setStitchUser(previousActiveUser);
      },
      onUserLoggedOut: (auth, loggedOutUser) => {
        console.log('onUserLoggedOut:', loggedOutUser);
        this.isConnectedSubject.next(this.isAuthenticated());
        console.log('isConnectedSubject.next:', this.isAuthenticated());
        //this.userSrvc.setStitchUser(loggedOutUser);
      },
      onUserRemoved: (auth, removedUser) => {
        console.log('onUserRemoved:', removedUser);
      },
      onUserLinked: (auth, linkedUser) => {
        console.log('onUserLinked:', linkedUser);
      },
      onListenerRegistered: (auth) => {
        console.log('onListenerRegistered');
      },
    };

    // Register the listener
    const {auth} = this.realmConnectSrvc.getRealmApp().currentUser.mongoClient("MONGO.APP_ID");
    auth.addAuthListener(this.myAuthListener);
*/
    // Register the listener
    //this.listenToCMUpdate();
  }

  // This method initialize the subscription to Update Custom Data Events
  /*
  listenToCMUpdate() {
    console.log('RealmAuthService::listenToCMUpdate() | method called');
    this.userSrvc.currentUser
    .pipe(
      takeUntil(this.destroy$),
    )
    .subscribe(x => {
      this.realmConnectSrvc.getMongoClient().auth.refreshCustomData();
      console.log('RealmAuthService::listenToCMUpdate() | just refreshed User Custom Data: ', this.getUser())
    });
  }
*/

/*
  ngOnDestroy() {
    console.log('RealmAuthService::ngOnDestroy() | method called');
    const {auth} = this.realmConnectSrvc.getMongoClient();
    auth.removeAuthListener(this.myAuthListener);

    this.destroy$.next();
    this.destroy$.complete();
  }
*/

/**
  * Utility Functions
*/


/**
  * @desc gets 'EmailPasswordAuth' object for Email/Password Authentication
  * @return Realm.Auth.EmailPasswordAuth - Object
*/
  getEmailPassClient(): any {
    return this.realmConnectSrvc.getRealmApp().emailPasswordAuth;
  }

/**
  * @desc return a 'RealmUser' object representing the currently active user,
  * or null if there is no active user
  * @return 'RealmUser' object or undefined
*/
  getUser(): RealmUser | null {
    return this.realmConnectSrvc.getRealmApp().currentUser;
  }

/**
  * @desc checks the logged in state of the user, can be anonymously or authentified
  * @return boolean - true or false
*/
  isLoggedIn(): boolean {
    return this.getUser()?.state === Realm.UserState.Active;
  }

/**
  * @desc checks if user isLoggedIn to Realm anonymously
  * @return boolean - true or false
*/
  isAnonymous(): boolean {
    // The 'providerType' property is Not yet Implemented in the Realm Web SDK.
    return this.getUser()?.providerType === 'anon-user';

    // WorkAround - Using Identities : Gets an array of identities for this user on MongoDB Realm Cloud.
    // Each element in the array is an object with properties userId and providerType.
    // Hoping the first element of the identities Array corresponds to the currentUser.
    // return this.getUser()?.identities[0]["providerType"] === 'anon-user';
  }

/**
  * @desc checks if user isAuthenticated to Realm as a real user, i.e. not as an anonymous user
  * @return boolean - true or false
*/
  isAuthenticated(): boolean | undefined {
    return (this.isLoggedIn() && !(this.isAnonymous()));
  }

/**
  * @desc checks if user isLoggedIn to Realm by Email
  * @return boolean - true or false
*/
  isLoggedWithEmail(): boolean {
    return this.getUser()?.providerType === 'local-userpass';
  }

/**
* @desc return a 'StitchAuth' object representing and controling the login state of
* a StitchAppClient
* @return 'StitchAuth' object
*/
/*
  getAuth(): Realm {
    return this.realmConnectSrvc.getRealmApp().authenticator.;
  }
*/

/**
* @desc return the Access Token representing the session token for the current active user.
* @return 'string' Acces Token
*/
  getAccessToken(): string | null | undefined {
    return this.getUser()?.accessToken;
  }


/**
* @desc return the Md5 checksum of the Access Token for the current active user.
* @return 'string' Md5 string of the Acces Token
*/
/*
  getMd5Token(): string | Int32Array {
    return Md5.hashStr(this.getAccessToken());
  }
*/

/**
* @desc return the storable object containing the Md5 checksum of the Access Token for the current active user.
* @return '{}' Object containing the Md5 string of the Acces Token
*/
/*
  getStorableMd5(): {} {
    return { 'md5_token':  this.getMd5Token() };
  }
*/

/**
* @desc return the storable object containing the Auth info to add to dataport documents.
* @return '{}' Object containing the Auth info to add to the documents
*/
/*
  getStorableAuth(): {} {
    return { 'user_id': this.getUser().id, 'md5_token':  this.getMd5Token() };
  }
*/

/**
  * @desc return a 'Readonly<Record<string,RealmUser>>' list of all users who have logged into this application,
  * except those that have been removed manually and anonymous users who have logged out.
  * Note: The list of users is a snapshot of the state when listUsers() is called.
  * The [[RealmUser]] in this list will not be updated if, e.g., a user's login state
  * changes after this is called.
  * @return 'Readonly<Record<string,RealmUser>>' list
*/
  getlistUsers(): Readonly<Record<string,RealmUser>> {
    return this.realmConnectSrvc.getRealmApp().allUsers;
  }

/**
  * @desc Logs out the active user and removes that user from the list of all users
  * associated with this application.
*/
  async removeCurrentUser(): Promise<void> {
    if (this.realmConnectSrvc.getRealmApp().currentUser)
      await this.realmConnectSrvc.getRealmApp().removeUser(<RealmUser>this.realmConnectSrvc.getRealmApp().currentUser);
    else
      console.debug('removeCurrentUser(): No active user');
  }

/**
  * Authentication Functions
*/

/**
  * @desc Logs the user through a classical email/password credential with redirect to
  * MONGO.PROVIDER_REDIRECT_URL in case of success
*/
  async loginByEmailPassword(_user: { email: string; password: string; }) {
    const credentials = Realm.Credentials.emailPassword(_user.email, _user.password);
    try {
      // Authenticate the user
      this.user = await this.realmConnectSrvc.getRealmApp().logIn(credentials);
      // `App.currentUser` updates to match the logged in user
      //assert(user.id === app.currentUser.id)
    } catch(err) {
      console.error("Failed to log in", err);
    }
  }

  async registerWithEmail(_data: { email: string; password: string; }): Promise<void> {
    console.debug('RealmAuthService:registerWithEmail | method called | _data: ', _data);

    return await this.getEmailPassClient().registerUser(_data.email, _data.password);
  }

  async confirmEmail(_data: any): Promise<void> {
    console.debug('RealmAuthService:confirmEmail | method called | _data: ', _data);

    return await this.getEmailPassClient().confirmUser(_data.token, _data.tokenId);
  }

/**
  * @desc Logs the user through a social provider with redirect to MONGO.PROVIDER_REDIRECT_URL
  * in case of success
  * Actually, implements Google & Facebook

  loginWithProvider(_providerName: string) : void {
    console.log('RealmAuthService::loginWithProvider() | method called');
    console.log ('RealmAuthService::loginWithProvider() - provider name: '+ _providerName);
    let providerCredential: any;
      if (_providerName === AUTH.GOOGLE_PROVIDER_NAME)
        providerCredential = new GoogleRedirectCredential(MONGO.PROVIDER_REDIRECT_URL);
      else if (_providerName === AUTH.FACEBOOK_PROVIDER_NAME)
        providerCredential = new FacebookRedirectCredential(MONGO.PROVIDER_REDIRECT_URL);
      else
        console.log('RealmAuthService::loginWithProvider() | unkown provider: ' + _providerName);

    Stitch.defaultAppClient.auth.loginWithRedirect(providerCredential);
  }
  */

  /**
  * @desc Resend confirmation email to user.
  * @return Promise<void>
  */
  async resendConfirmationEmail(_email: string): Promise<void> {
    console.debug('RealmAuthService:resendConfirmationEmail() | method called | email: ' + _email);

    return await this.getEmailPassClient().resendConfirmationEmail(_email);

  }

  /**
  * @desc Claim user's password reset.
  * @return Promise<void>
  */
  async sendResetPassword(_email: string): Promise<void> {
    console.debug('RealmAuthService::sendResetPassword() | method called | _email: ', _email);

    return await this.getEmailPassClient().sendResetPasswordEmail(_email);

  }

  /**
  * @desc Reset user's password.
  * @return Promise<void>
  */
  async resetPassword(_data: { password: string; token: string; tokenId: string; }): Promise<void> {
    console.debug('RealmAuthService::resetPassword() | method called | _data: ', _data);
    // Reset the user's password
    await this.getEmailPassClient().resetPassword(_data.password, _data.token, _data.tokenId);
  }

/**
  * @desc Logs out the active user if authenticated and connects back anonymously.
  * @return Promise<void>
*/
  async logout (): Promise<void> {
    console.log('RealmAuthService::logout() | method call');
    // let _stitchUser = this.stitchMongoSrvc.getUser();
    if (this.isAuthenticated()) {
      console.log('RealmAuthService::logout() - user will log out ', this.getUser());
      // "removeUser()" : Logs out the active user and removes that user from the list of all
      // users associated with this application as returned by StitchAuth.listUsers.
      /*
      this.realmConnectSrvc.getMongoClient().auth.logout().then( () => {
        // Login as an anonymous user
        this.realmConnectSrvc.getMongoClient().auth.loginWithCredential(new AnonymousCredential()).then(_user => {
          // this.setStitchUser(_user);
          console.log('RealmAuthService::logout() - Logged In to Stitch as an anonymous with id:'+ _user.id);
        })
      });
      */
      // Logout the authenticted user
      await this.getUser()?.logOut();
      // Re-Login to Stitch with Anonymous credentials
      const _user = await this.realmConnectSrvc.getRealmApp().logIn(Realm.Credentials.anonymous());
      console.log('RealmAuthService::logout() - Logged In to Stitch as an anonymous with id:' + _user.id);
    }
    else
      console.log('RealmAuthService::logout() - user cannot logout because it is not authenticated: ', this.getUser());
  }

}
