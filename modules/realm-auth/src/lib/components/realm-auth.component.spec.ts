import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealmAuthComponent } from './realm-auth.component';

describe('RealmAuthComponent', () => {
  let component: RealmAuthComponent;
  let fixture: ComponentFixture<RealmAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealmAuthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RealmAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
